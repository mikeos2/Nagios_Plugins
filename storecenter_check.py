#!/usr/bin/python
#   storecenter_check.py - A Python plugin for Nagious to retrieve SNMP v3 data from a Lenovo ix2 NAS.
#   Copyright (C) 2016 Michael Greene <mikeos2@gmail.com>
#
#   Based on original work by Claudio Kuenzler's (www.claudiokuenzler.com) check_storcenter.sh script.
#   This program is free software: you can redistribute it and/or modify it
#   under the terms of the GNU General Public License version 3, as published
#   by the Free Software Foundation.
#
#   This program is distributed in the hope that it will be useful, but
#   WITHOUT ANY WARRANTY; without even the implied warranties of
#   MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License along
#   with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys
import datetime
import argparse

# Nagios Plugin Return Codes
# https://nagios-plugins.org/doc/guidelines.html#PLUGOUTPUT
Nagios_OK = 0              # define the exit code if status is OK
Nagios_WARNING = 1         # define the exit code if status is Warning
Nagios_CRITICAL = 2        # define the exit code if status is Critical
Nagios_UNKNOWN = 3         # define the exit code if status is Unknown

try:
    from pysnmp.hlapi import *
except ImportError:
    print "Error: (" + str(Nagios_UNKNOWN) + ") install pysnmp!"
    sys.exit(Nagios_UNKNOWN)

# ************* SNMP Calls *************
# This is the class that interfaces with pysnmp
#
class SnmpCalls:

    def __init__(self, host, user, password, oprtype, privacy, warn, critical):

        self.snmpengine = SnmpEngine()
        if privacy:
            self.usmuserdata = UsmUserData(user, password, password)
        else:
            self.usmuserdata = UsmUserData(user, password)

        try:
            self.udptransporttarget = UdpTransportTarget((host, 161), timeout=3, retries=1)
        except:
            if oprtype == 0:
                print "Error: host issue"
                sys.exit(Nagios_UNKNOWN)

        self.opr_type = oprtype

        if warn == -1:
            self.warn = warn
        else:
            self.warn = int(warn)

        if critical == -1:
            self.critical = critical
        else:
            self.critical = int(critical)

    # getcmd to pull one data item
    def _getcmd_data(self, mib):
        return next(getCmd(self.snmpengine, self.usmuserdata, self.udptransporttarget,
                   ContextData(), mib))

    # getnextcmd to pull multi data items
    def _getnextcmd_data(self, mib):
        return next(nextCmd(self.snmpengine, self.usmuserdata, self.udptransporttarget,
                ContextData(),
                mib, lookupMib=False))

    def _get_result(self, errorIndication, errorStatus, errorIndex, varBinds):
        """Put results in table.

        This is the stupidest shit I have ever heard, errorIndication is a type instance
        which I read to mean "because the library is using old-style classes" according
        to a Google search, WTF. So I have to do errorIndication.__class__.__name__
        This returns the class name located in pysnmp/proto/errind.py and is good enough,
        but what a pain in the ass. Here is a description of the returns:

        Non-empty errorIndication string indicates SNMP engine-level error.

        The pair of errorStatus and errorIndex variables determines SNMP PDU-level
        error. These are instances of pyasn1 Integer class. If errorStatus evaluates
        to true, this indicates SNMP PDU error caused by Managed Object at position
        errorIndex-1 in varBinds. Doing errorStatus.prettyPrint() would return an
        explanatory text error message.

        The varBinds is a sequence of Managed Objects. Those found in response are
        bound by position to Managed Object names passed in request.

        This a quick BS fix:
        """
        # Handle errorIndication
        if errorIndication.__class__.__name__ != "NoneType":
            return Nagios_UNKNOWN, errorIndication.__class__.__name__

        # Handle errorstatus
        if errorStatus:
            return Nagios_UNKNOWN, "PDUError"

        # Should be good at this point with a good return - I hope
        # Okay, varBinds is a list, so I am going to try and convert to string
        # and move on.  However, more maybe needed here for mult returned results
        return Nagios_OK, str(varBinds[0])[(str(varBinds[0]).find('=') + 2):]

    # ******** Get raid status and return as a string ********
    def nas_raidstatus(self):
        try:
            errorIndication, errorStatus, errorIndex, varBinds = self._getcmd_data(
                ObjectType(ObjectIdentity('SNMPv2-SMI', 'enterprises', '11369', '10', '4', '1', '0')))
            ret_state, ret_description = self._get_result(errorIndication, errorStatus, errorIndex, varBinds)
        except:
            print "Error: Bad SNMP option in raidstatus _getcmd_data call"
            sys.exit(Nagios_UNKNOWN)

        if ret_description != "NORMAL":
            if (ret_description == "REBUILDING") or (ret_description == "REBUILDFS"):
                ret_state = Nagios_WARNING
            else:
                # if not the above then it must be DEGRADED or FAULTED
                ret_state = Nagios_CRITICAL

        return ret_state, ret_description

    # ******** Gets NAS uptime and returns as a string ********
    def nas_uptime(self):
        try:
            errorIndication, errorStatus, errorIndex, varBinds = self._getcmd_data(
                ObjectType(ObjectIdentity('HOST-RESOURCES-MIB', 'hrSystemUptime', '0')))
            ret_state, ret_description = self._get_result(errorIndication, errorStatus, errorIndex, varBinds)
        except:
            print "Error: Bad SNMP option in uptime _getcmd_data call"
            sys.exit(Nagios_UNKNOWN)

        return ret_state, str(datetime.timedelta(seconds=(int(ret_description)/100)))

    # ******** Gets temps and return as string ********
    def nas_temp(self):

        # Get CPU temp
        try:
            errorIndication, errorStatus, errorIndex, varBinds = self._getcmd_data(
                ObjectType(ObjectIdentity('SNMPv2-SMI', 'enterprises', '11369', '10', '6', '2', '1', '3', '1')))
            ret_state, ret_description = self._get_result(errorIndication, errorStatus, errorIndex, varBinds)
        except:
            print "Error: Bad SNMP option in cpu temp _getcmd_data call"
            sys.exit(Nagios_UNKNOWN)

        cpu_temp = int(ret_description)

        # I want it F too
        f_temp = cpu_temp * (9/5) + 32
        result = "CPU/Motherboard: " + str(ret_description) + "C (" + str(f_temp) + "F)"

        # Get Motherboard temp
        try:
            errorIndication, errorStatus, errorIndex, varBinds = self._getcmd_data(
                ObjectType(ObjectIdentity('SNMPv2-SMI', 'enterprises', '11369', '10', '6', '2', '1', '3', '2')))
            ret_state, ret_description = self._get_result(errorIndication, errorStatus, errorIndex, varBinds)
        except:
            print "Error: Bad SNMP option in mb temp _getcmd_data call"
            sys.exit(Nagios_UNKNOWN)

        mb_temp = int(ret_description)

        # I want it F too
        f_temp = mb_temp * (9/5) + 32
        result = result + " /" + str(ret_description) + "C (" + str(f_temp) + "F)"

        # Idle temps are usually CPU/Motherboard: 59C (91F) /49C (81F)
        if self.warn != -1:
            if (mb_temp > self.warn) or (cpu_temp > self.warn):
                ret_state = Nagios_WARNING

        if self.critical != -1:
            if (mb_temp > self.critical) or (cpu_temp > self.critical):
                ret_state = Nagios_CRITICAL

        return ret_state, result, cpu_temp, mb_temp

    # ******** Gets fan  and return as string ********
    def nas_fan(self):

        # Get fan RPM
        try:
            errorIndication, errorStatus, errorIndex, varBinds = self._getcmd_data(
                ObjectType(ObjectIdentity('SNMPv2-SMI', 'enterprises', '11369', '10', '6', '1', '1', '3', '1')))
            ret_state, ret_description = self._get_result(errorIndication, errorStatus, errorIndex, varBinds)
        except:
            print "Error: Bad SNMP option in fan _getcmd_data call"
            sys.exit(Nagios_UNKNOWN)

        # The fan speed is usually ~770-779 RPM
        fan_rpm = int(ret_description)

        if self.warn != -1:
            if fan_rpm < self.warn:
                ret_state = Nagios_WARNING

        if self.critical != -1:
            if fan_rpm < self.critical:
                ret_state = Nagios_CRITICAL

        return ret_state, str(ret_description), fan_rpm

    # Gets disks
    def nas_disks(self):

        # This is my quick way to get number of drives in the system, there might be a better way
        # to do this in pysnmp, but I do not have time to figure it out right now. Brut force way of checking
        # for up to 4 drives.

        installed_disks = 0

        disk_calls=[
            ObjectType(ObjectIdentity('SNMPv2-SMI', 'enterprises', '11369', '10', '4', '3', '1', '1', '1')),
            ObjectType(ObjectIdentity('SNMPv2-SMI', 'enterprises', '11369', '10', '4', '3', '1', '1', '2')),
            ObjectType(ObjectIdentity('SNMPv2-SMI', 'enterprises', '11369', '10', '4', '3', '1', '1', '3')),
            ObjectType(ObjectIdentity('SNMPv2-SMI', 'enterprises', '11369', '10', '4', '3', '1', '1', '4'))]

        for temp in range(0,4):
            try:
                errorIndication, errorStatus, errorIndex, varBinds = self._getcmd_data(disk_calls[temp])
                ret_state, ret_description = self._get_result(errorIndication, errorStatus, errorIndex, varBinds)

                # This is ridiculous, the error returns suck so I just check if the return length
                # is greater than 1. I should have just used snmpwalk instead of pysnmp
                if len(ret_description) == 1:
                    installed_disks += 1

            except:
                print "Error: Bad SNMP option in disks _getncmd_data call"
                sys.exit(Nagios_UNKNOWN)

        ret_msg = list()
        disk_state = Nagios_OK # removes BS pyCharm warning
        raid_state = Nagios_OK # removes BS pyCharm warning

        disk_status=[
            ObjectType(ObjectIdentity('SNMPv2-SMI', 'enterprises', '11369', '10', '4', '3', '1', '4', '1')),
            ObjectType(ObjectIdentity('SNMPv2-SMI', 'enterprises', '11369', '10', '4', '3', '1', '4', '2')),
            ObjectType(ObjectIdentity('SNMPv2-SMI', 'enterprises', '11369', '10', '4', '3', '1', '4', '3')),
            ObjectType(ObjectIdentity('SNMPv2-SMI', 'enterprises', '11369', '10', '4', '3', '1', '4', '4'))]

        for temp in range(0,installed_disks):
            try:
                errorIndication, errorStatus, errorIndex, varBinds = self._getcmd_data(disk_status[temp])
                ret_state, ret_description = self._get_result(errorIndication, errorStatus, errorIndex, varBinds)

                # If status is not NORMAL or MISSING then it must be
                # FOREIGN or FAULTED
                if (ret_description == "NORMAL") or (ret_description == "MISSING"):
                    disk_state = Nagios_OK
                elif (ret_description == "FOREIGN"):
                    disk_state = Nagios_WARNING
                else:
                    # must be faulted
                    disk_state = Nagios_CRITICAL

                # Nothing fancy, highest state determines overall condition
                if disk_state > raid_state:
                    raid_state = disk_state

                ret_msg.append("Disk " + str(temp) + " " + ret_description)

            except:
                print "Error: Bad SNMP option in disks _getncmd_data call"
                sys.exit(Nagios_UNKNOWN)

        return raid_state, ret_msg

        # ******** Gets fan  and return as string ********

    def nas_mem(self):

        # Get fan mem
        try:
            errorIndication, errorStatus, errorIndex, varBinds = self._getcmd_data(
                ObjectType(ObjectIdentity('1', '3', '6', '1', '4', '1', '2121','4', '5', '0')))
            ret_state, ret_description = self._get_result(errorIndication, errorStatus, errorIndex, varBinds)
        except:
            print "Error: Bad SNMP option in fan _getcmd_data call"
            sys.exit(Nagios_UNKNOWN)

        print ret_description

# ********** Main Entry **********
#
def main(snmpcalls):

    if snmpcalls.opr_type == 0 or snmpcalls.opr_type == 1:
        ret_status, ret_type = snmpcalls.nas_uptime()
        if snmpcalls.opr_type == 0:
            print "System Uptime: (" + str(ret_status) + ") " + ret_type
        else:
            print "System Uptime: " + ret_type
            return ret_status

    if snmpcalls.opr_type == 0 or snmpcalls.opr_type == 2:
        ret_status, ret_type, cpu_temp, mb_temp = snmpcalls.nas_temp()
        if snmpcalls.opr_type == 0:
            print "System Temp: (" + str(ret_status) + ") " + ret_type
        else:
            print "System Temp: " + ret_type + " | nas_cpu_temp=" + str(cpu_temp) + \
                  ", nas_mb_temp=" + str(mb_temp)
            return ret_status

    if snmpcalls.opr_type == 0 or snmpcalls.opr_type == 3:
        ret_status, ret_type, fan_rpm = snmpcalls.nas_fan()
        if snmpcalls.opr_type == 0:
            print "System Fan: (" + str(ret_status) + ") " + ret_type + " RPM"
        else:
            print "System Fan: " + ret_type + " RPM | nas_fan_rpm=" + str(fan_rpm)
            return ret_status

    if snmpcalls.opr_type == 0 or snmpcalls.opr_type == 4:
        ret_status, ret_type = snmpcalls.nas_raidstatus()
        if snmpcalls.opr_type == 0:
            print "Raid status: (" + str(ret_status) + ") " + ret_type
        else:
            print "Raid status: " + ret_type
            return ret_status

    if snmpcalls.opr_type == 0 or snmpcalls.opr_type == 5:
        ret_status, ret_type = snmpcalls.nas_disks()

        for temp in range(0,len(ret_type)):
            if snmpcalls.opr_type == 0:
                print "Disk Status: " + ret_type[temp] + " Return " + str(ret_status)
            else:
                print "Disk Status: " + ret_type[temp]

        if snmpcalls.opr_type == 5:
            return ret_status

        if snmpcalls.opr_type == 0 or snmpcalls.opr_type == 6:
            ret_status, ret_type = snmpcalls.nas_mem()


if __name__ == "__main__":

    parser = argparse.ArgumentParser(
        description='Nagios command to retrieve Lenovo ix2 NAS SNMP data.',
        usage='%(prog)s host user password type [-p] [-w warning] [-c critical]',
        epilog="Types: disk,raid,cpu,mem,info")
    parser.add_argument('host', help='hostname')
    parser.add_argument('user', help='user (to be defined in snmp settings on Storcenter)')
    parser.add_argument('password', help='password (to be defined in snmp settings on Storcenter)')
    parser.add_argument('type', help='Type to check')
    parser.add_argument('-p', '--privacy', default='False',
                        action='store_true',
                        help='Flag enables privacy uses same password (optional)')
    parser.add_argument('-w', '--warn',
                        help='Warning Threshold (optional)', default=-1)
    parser.add_argument('-c', '--critical',
                        help='Critical Threshold (optional)', default=-1)
    args = parser.parse_args()

    #  Just use brut force option selection
    if args.type == "debug":
        opr_type = 0                # do everything
        # no range checks for all
        args.warn = -1
        args.critical = -1
    elif args.type == "uptime":
        opr_type = 1                # uptime
    elif args.type == "temp":
        opr_type = 2                # temp
    elif args.type == "fan":
        opr_type = 3                # fan
    elif args.type == "raid":
        opr_type = 4                # raid status
    elif args.type == "disks":
        opr_type = 5                # raid disk status
    elif args.type == "mem":
        opr_type = 6                # raid disk status
    else:
        print "Error: Bad operation type (" + args.type + ")."  # bad operation
        sys.exit(Nagios_UNKNOWN)

    # Need to check, arg returned is a string rather than bool
    if args.privacy != True: args.privacy = False

    options = SnmpCalls(args.host, args.user, args.password, opr_type, args.privacy, args.warn, args.critical)

    sys.exit(main(options))
